package bopJunit;

 

import static org.junit.Assert.assertEquals;

 

import org.junit.After;
import org.junit.Before;

 

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

 

//@Disabled
public class section4 {
    WebDriver aa;
    
    @Before
    public void startApp() {
        System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
        aa = new ChromeDriver();
        aa.get("http://shop.demoqa.com/");
    }
    
    @BeforeEach 
    public void beforeRepeat() {
        System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
        aa = new ChromeDriver();
        aa.get("http://shop.demoqa.com/");
    }
    
    //@Test(timeout = 1000)
    @RepeatedTest(2)
    public void test1() {
        assertEquals("http://shop.demoqa.com/",aa.getCurrentUrl());
    }
    
    //@Disabled("test case 2 is disabled")
    @Test (timeout = 1000)
    public void test2() {
        assertEquals("Shopttulls",aa.getTitle());
    }
    
    @AfterEach
    public void afterRepeat() {
        aa.close();
    }
    @After
    public void closeApp() {
        aa.close();
    }
}