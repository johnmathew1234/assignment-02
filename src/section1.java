import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

//import com.sun.tools.javac.util.Assert;

public class qq {

	WebDriver dr;
	@Before
	public void launchsite()
	{
		 System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
	        dr=new ChromeDriver();
	        dr.get("http://shop.demoqa.com/");
	}
	@After
	  public void CloseApp()
    {
        dr.close();
    }
	@Test
	public void TC1()
	{
		List<WebElement> links=dr.findElements(By.tagName("a"));
		boolean found=false;
		for(WebElement aa:links)
		{
			if(aa.getAttribute("href").equals("http://shop.demoqa.com/"))
			{
				found=true;
				break;
			}
				
		}
		Assert.assertTrue(found);
		}
	@Test
	public void TC2()
	{
		boolean ab=dr.getCurrentUrl().contains("http://shop.demoqa.com/");
		Assert.assertTrue(ab);
		
	}
	
}
